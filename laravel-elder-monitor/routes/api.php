<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    $user = App\User::with(['role', 'caretaker', 'elders', 'activities' => function($q) {
        $q->whereBetween('created_at', [Carbon\Carbon::now()->subDay(), Carbon\Carbon::now()]);
    }, 'heart_rates' => function($q) {
        $q->whereBetween('created_at', [Carbon\Carbon::now()->subDay(), Carbon\Carbon::now()]);
    },
    'sleep_hours' => function($q) {
        $q->whereBetween('created_at', [Carbon\Carbon::now()->subDay(), Carbon\Carbon::now()]);
    }])->findOrFail($request->user()->id);
    return $user;
});

Route::group(['middleware' => 'auth:sanctum'], function() {
    Route::put('/user', 'UserController@update');
    Route::get('/elders', 'UserController@elders_for_home_page');
    Route::get('/elder-for-profile/{id}', 'UserController@elder_for_profile');
    Route::get('/elder/{id}', 'UserController@elder');

    Route::get('/activities/{userId}', 'ActivityController@index');
    Route::get('/hearts/{userId}', 'HeartController@index');
    Route::get('/sleeps/{userId}', 'SleepController@index');

    Route::put('/assign-caretaker', 'UserController@assign_caretaker');
    Route::post('/change-role', 'UserController@change_role');
});
