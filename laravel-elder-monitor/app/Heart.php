<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Heart extends Model
{
    protected $fillable = [ 'user_id', 'heart_rate' ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
