<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $guard_name = 'sanctum';

    protected $fillable = [
        'name', 'username', 'email', 'phone', 'gender', 'password',
        'blood_type', 'height', 'weight'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role() {
        return $this->belongsTo('App\Role', 'role_id');
    }

    public function activities() {
        return $this->hasMany('App\Activity', 'user_id');
    }

    public function heart_rates() {
        return $this->hasMany('App\Heart', 'user_id');
    }

    public function sleep_hours() {
        return $this->hasMany('App\Sleep', 'user_id');
    }

    public function caretaker() {
        return $this->belongsTo(self::class, 'caretaker_id');
    }

    public function elders() {
        return $this->hasMany(self::class, 'caretaker_id');
    }

}
