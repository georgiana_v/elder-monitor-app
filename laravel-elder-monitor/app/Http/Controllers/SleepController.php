<?php

namespace App\Http\Controllers;
use App\Sleep;
use Carbon\Carbon;

use Illuminate\Http\Request;

class SleepController extends Controller
{
    public function index($userId) {
        $sleeps = Sleep::where('user_id', $userId)->where('created_at', '<', Carbon::now())->orderByDesc('created_at')->get();

        return response()->json($sleeps);
    }
}
