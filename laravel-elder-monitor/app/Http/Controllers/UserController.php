<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserController extends Controller
{
    public function update(Request $request) {
        $user = User::findOrFail($request->id);

        if($user) {
            $user->name = $request->name;
            $user->username = $request->username;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->gender = $request->gender;
            $user->blood_type = $request->blood_type;
            $user->weight = $request->weight;
            $user->height = $request->height;

            if($request->password != '') {
                $user->password = Hash::make($request->password);
            }

            $user->update();

            return response()->json($user);
        } else {
            return response()->json(['error' => 'User has not been found'], 422);
        }
    }

    public function elders_for_home_page() {
        $elders = User::with(['heart_rates' => function($q) {
            $q->whereBetween('created_at', [Carbon::now()->subDays(1), Carbon::now()->addDays(1)]);
        }, 'activities' => function($q) {
            $q->whereBetween('created_at', [Carbon::now()->subDays(1), Carbon::now()->addDays(1)]);
        }, 'sleep_hours' => function($q) {
            $q->whereBetween('created_at', [Carbon::now()->subDays(1), Carbon::now()->addDays(1)]);
        }, 'role', 'caretaker', 'elders'])->get();

        return response()->json($elders);
    }

    public function elder_for_profile($id) {
        $elder = User::with(['heart_rates' => function($q) {
            $q->whereBetween('created_at', [Carbon::now()->subDay(), Carbon::now()]);
        }, 'activities' => function($q) {
            $q->whereBetween('created_at', [Carbon::now()->subDay(), Carbon::now()]);
        }, 'sleep_hours' => function($q) {
            $q->whereBetween('created_at', [Carbon::now()->subDay(), Carbon::now()]);
        }])->find($id);

        return response()->json($elder);
    }

    public function elder($id) {
        $elder = User::with(['heart_rates' => function($q) {
            $q->where('created_at', '<', Carbon::now());
        }, 'activities' => function($q) {
            $q->where('created_at', '<', Carbon::now());
        }, 'sleep_hours' => function($q) {
            $q->where('created_at', '<', Carbon::now());
        }, 'caretaker'])->find($id);

        return response()->json($elder);
    }

    public function assign_caretaker(Request $request) {
        $elder = User::findOrFail($request->elderId);

        $elder->caretaker_id = $request->caretakerId;
        $elder->update();

        return response()->json('Caretaker was assigned succesfully to the elder!');
    }

    public function change_role(Request $request) {
        $user = User::findOrFail($request->userId);

        if($request->roleId == 2 AND $user->caretaker_id != null) {
            $user->caretaker_id == null;
        }

        $user->role_id = $request->roleId;
        $user->update();

        return response()->json('Role changed succesfully');
    }
}
