<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Activity;
use Carbon\Carbon;

class ActivityController extends Controller
{
    public function index($userId) {
        $activities = Activity::where('user_id', $userId)->where('created_at', '<', Carbon::now())->orderByDesc('created_at')->get();

        return response()->json($activities);
    }
}
