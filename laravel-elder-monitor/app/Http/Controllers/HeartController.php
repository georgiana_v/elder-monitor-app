<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Heart;
use Carbon\Carbon;

class HeartController extends Controller
{
    public function index($userId) {
        $hearts = Heart::where('user_id', $userId)->where('created_at', '<', Carbon::now())->orderByDesc('created_at')->get();

        return response()->json($hearts);
    }
}
