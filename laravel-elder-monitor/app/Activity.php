<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = [ 'user_id', 'hour_steps', 'daily_steps', 'distance' ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
