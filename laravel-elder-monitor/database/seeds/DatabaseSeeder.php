<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Carbon\Carbon;
use App\User;
use App\Role;
use App\Activity;
use App\Heart;
use App\Sleep;

class DatabaseSeeder extends Seeder
{
    public function run(Faker $faker)
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'username' => 'admin',
            'phone' => $faker->e164PhoneNumber,
            'gender' => 'female',
            'password' => Hash::make('password'),
            'blood_type' => $faker->randomElement(['A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-']),
            'height' => $faker->numberBetween(145, 175),
            'weight' => $faker->numberBetween(45, 85),
            'role_id' => '2',
            'remember_token' => Str::random(10),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        factory(User::class, 50)->create();

        Role::create([
            'name' => 'Elder'
        ]);
        Role::create([
            'name' => 'Caretaker'
        ]);

        $users = User::all();

        foreach($users as $user) {

            $startDate = Carbon::now()->subDays(10);

            do{
                $steps = 0;
                $activityIds = [];

                $hearts = 0;
                $minHeart = 0;
                $maxHeart = 0;
                $heartIds = [];

                foreach([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23] as $hour) {

                    $date = Carbon::create($startDate->format('Y'), $startDate->format('m'), $startDate->format('d'), $hour, 0, 0);
                    $heartRate = $faker->numberBetween(75, 150);

                    if($hour == 0){
                        $minHeart = $heartRate;
                        $maxHeart = $heartRate;
                    }

                    $hearts += $heartRate;

                    if($minHeart > $heartRate) {
                        $minHeart = $heartRate;
                    }
                    if($maxHeart < $heartRate) {
                        $maxHeart = $heartRate;
                    }

                    $heart = Heart::create([
                        'user_id' => $user->id,
                        'heart_rate' => $heartRate,
                        'created_at' => $date,
                        'updated_at' => $date
                    ]);

                    array_push($heartIds, $heart->id);

                    if(in_array($startDate->format('H'), [8,9,10,11,12,13,14,15,16,17,18,19,20,21])) {
                        $hourSteps = $faker->numberBetween(0, 250);

                        $steps += $hourSteps;

                        $activity = Activity::create([
                            'user_id' => $user->id,
                            'hour_steps' => $hourSteps,
                            'hour_distance' => $hourSteps / 1312,
                            'created_at' => $date,
                            'updated_at' => $date
                        ]);

                        array_push($activityIds, $activity->id);
                    }
                }

                $heartAverage = number_format($hearts / 24, 0);

                foreach($activityIds as $id){
                    $activity = Activity::find($id);

                    $activity->daily_steps = $steps;
                    $activity->daily_distance = $steps / 1312;
                    $activity->update();
                }

                foreach($heartIds as $id) {
                    $heart = Heart::find($id);

                    $heart->daily_average = $heartAverage;
                    $heart->daily_min = $minHeart;
                    $heart->daily_max = $maxHeart;
                    $heart->update();
                }

                Sleep::create([
                    'user_id' => $user->id,
                    'sleeping_hours' => $faker->numberBetween(1,10) == 7 ? $faker->numberBetween(7,10) : $faker->numberBetween(10,12),
                    'created_at' => Carbon::create($startDate->format('Y'), $startDate->format('m'), $startDate->format('d'), 0, 0, 0),
                    'updated_at' => Carbon::create($startDate->format('Y'), $startDate->format('m'), $startDate->format('d'), 0, 0, 0),
                ]);

                $startDate->addDays(1);
            } while (!$startDate->isSameDay(Carbon::now()->addDays(10)));
        }
    }
}
