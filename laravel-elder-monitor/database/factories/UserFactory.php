<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

$factory->define(User::class, function (Faker $faker) {
    $gender = $faker->randomElement(['male', 'female']);
    $name = $faker->name;
    return [
        'name' => $name,
        'email' => $faker->unique()->email,
        'username' => strtolower(explode(' ', $name)[0]).'_'.strtolower(explode(' ', $name)[1]),
        'phone' => $faker->e164PhoneNumber,
        'gender' => $gender,
        'password' => Hash::make('password'),
        'blood_type' => $faker->randomElement(['A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-']),
        'height' => $gender == 'female' ? $faker->numberBetween(145, 175) : $faker->numberBetween(165, 200),
        'weight' => $gender == 'female' ? $faker->numberBetween(45, 85) : $faker->numberBetween(75, 125),
        'role_id' => '1',
        'remember_token' => Str::random(10),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
    ];
});
