import { User } from './user.model';
import { Moment } from 'moment';

export class Sleep {
  constructor(
    public id: number,
    public user_id: number,
    public user: User,
    public sleeping_hours: number,
    public created_at: Moment,
    public updated_at: Moment
  ) {}
}
