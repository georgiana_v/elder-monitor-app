import { User } from './user.model';
import { Moment } from 'moment';

export class Heart {
  constructor(
    public id: number,
    public user_id: number,
    public user: User,
    public heart_rate: number,
    public daily_average: number,
    public daily_min: number,
    public daily_max: number,
    public created_at: Moment,
    public updated_at: Moment,
  ) {}
}
