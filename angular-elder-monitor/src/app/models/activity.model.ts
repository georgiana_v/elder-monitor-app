import { User } from './user.model';
import { Moment } from 'moment';

export class Activity {
  constructor(
    public id: number,
    public user_id: number,
    public user: User,
    public hour_steps: number,
    public daily_steps: number,
    public hour_distance: number,
    public daily_distance: number,
    public created_at: Moment,
    public updated_at: Moment
  ) {}
}
