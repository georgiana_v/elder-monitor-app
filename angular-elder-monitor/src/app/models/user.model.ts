import { Role } from './role.model';
import { Moment } from 'moment';
import { Activity } from './activity.model';
import { Heart } from './heart.model';
import { Sleep } from './sleep.model';

export class User {
  constructor(
    public id: number,
    public name: string,
    public username: string,
    public email: string,
    public phone: string,
    public gender: string,
    public blood_type: string,
    public height: number,
    public weight: number,
    public role_id: number,
    public caretaker_id: number,
    public caretaker: User,
    public elders: User[],
    public role: Role,
    public created_at: Moment,
    public updated_at: Moment,
    public activities: Activity[],
    public heart_rates: Heart[],
    public sleep_hours: Sleep[],
  ) {}
}
