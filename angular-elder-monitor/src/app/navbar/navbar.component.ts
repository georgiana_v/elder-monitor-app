import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isAuth: Observable<boolean>;
  isAdmin: boolean;
  adminMenu = false;

  constructor(
    public authService: AuthService,
    public router: ActivatedRoute
  ) {
    const user = JSON.parse(localStorage.getItem('user'));

    if(user && user.role.name == 'Caretaker'){
      this.isAdmin = true;
    } else {
      this.isAdmin = false;
    }
   }

  ngOnInit(): void {
    this.isAuth = this.authService.isAuth;
  }

  onLogout() {
    this.authService.logout();
  }
}
