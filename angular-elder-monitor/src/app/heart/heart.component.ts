import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef, HostListener } from '@angular/core';

import * as moment from 'moment/moment';
import { MdbTableDirective, MdbTablePaginationComponent } from 'ng-uikit-pro-standard';
import { Heart } from '../models/heart.model';
import { HeartService } from './heart.service';

@Component({
  selector: 'app-heart',
  templateUrl: './heart.component.html',
  styleUrls: ['./heart.component.scss']
})
export class HeartComponent implements OnInit, AfterViewInit {
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild('row', { static: true }) row: ElementRef;
  hourlyChart = true;
  isLoading = false;

  hearts: Heart[] = [];

  public chartType: string = 'line';

  public chartDatasets: {data: any[], label: string }[] = [
    { data: [], label: 'Heart Rate / Hour'},
    { data: [], label: 'Average BPM / Day'}
  ];

  public chartLabels: Array<any> = [];

  public chartOptions: any = {
    responsive: true
  };

  public chartColors: Array<any> = [
    {
      backgroundColor: 'rgba(244, 67, 54, 0.3)',
      borderColor: 'rgba(244, 67, 54, 0.7)',
      borderWidth: 2,
    },
    {
      backgroundColor: 'rgba(205, 220, 57, 0.3)',
      borderColor: 'rgba(205, 220, 57, 0.7)',
      borderWidth: 2,
    }
  ];

  elements: any = [];
  headElements = ['id', 'heart rate', 'minimum BPM / Day', 'maximum BPM / Day', 'average BPM / Day', 'date - Time'];

  searchText: string = '';
  previous: string;

  maxVisibleItems: number = 25;

  constructor(
    private cdRef: ChangeDetectorRef,
    public heartService: HeartService
  ) { }

  @HostListener('input') oninput() {
    this.mdbTablePagination.searchText = this.searchText;
  }

  ngOnInit(): void {
    this.isLoading = true;

    const user = JSON.parse(localStorage.getItem('user'));

    this.heartService.getUserHearts(user.id).subscribe(hearts => {
      this.hearts = hearts;

      hearts.forEach(heart => {
        this.elements.push({
          id: heart.id,
          heart_rate: heart.heart_rate,
          min: heart.daily_min,
          max: heart.daily_max,
          average: heart.daily_average,
          date_time:  moment(heart.created_at).format('MMM DD - HH:mm')
        })
      })

      this.mdbTable.setDataSource(this.elements);
      this.elements = this.mdbTable.getDataSource();
      this.previous = this.mdbTable.getDataSource();

      this.changeChartData();

      this.isLoading = false;
    })
  }

  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(this.maxVisibleItems);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  searchItems() {
    const prev = this.mdbTable.getDataSource();

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();

    this.mdbTable.searchDataObservable(this.searchText).subscribe(() => {
      this.mdbTablePagination.calculateFirstItemIndex();
      this.mdbTablePagination.calculateLastItemIndex();
    });
  }

  changeChartData() {
    if(this.hourlyChart) {
      const todayHearts = this.hearts.filter(heart => moment().format('DM') == moment(heart.created_at).format('DM'));

      this.chartDatasets[0].data = todayHearts.map(heart => heart.heart_rate);
      this.chartDatasets[0].label = 'Heart Rate / Hour';
      this.chartDatasets[1].data = todayHearts.map(heart => heart.daily_average);
      this.chartDatasets[1].label = 'Average BPM / Day';
      this.chartLabels = todayHearts.map(heart => moment(heart.created_at).format('HH:mm'));
    } else {
      this.chartDatasets[0].label = 'Average BPM / Day';
      this.chartDatasets[1].label = 'Total Average BPM';
      this.chartLabels = [...new Set(this.hearts.map(heart => moment(heart.created_at).format('DD MMM')))];
      this.chartDatasets[0].data = [];
      this.chartLabels.forEach(day => {
        this.chartDatasets[0].data.push(this.hearts.filter(heart => moment(heart.created_at).format('DD MMM') == day)[0].daily_average);
      })
      const totalAverage = +this.chartDatasets[0].data.reduce((a, b) => +a + +b, 0) / +this.chartDatasets[0].data.length;
      this.chartDatasets[1].data = [];
      this.chartLabels.forEach(day => {
        this.chartDatasets[1].data.push(totalAverage.toFixed(0));
      })
    }
    this.hourlyChart = !this.hourlyChart;
  }
}
