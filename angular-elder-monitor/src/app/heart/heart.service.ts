import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Heart } from '../models/heart.model';

@Injectable({
  providedIn: 'root'
})
export class HeartService {
  baseURL = 'http://127.0.0.1:8000';

  constructor(
    public http: HttpClient
  ) { }

  getUserHearts(id: number) {
    return this.http.get<Heart[]>(this.baseURL + '/api/hearts/' + id);
  }
}
