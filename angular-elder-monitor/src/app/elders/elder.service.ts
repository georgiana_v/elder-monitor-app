import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class ElderService {
  baseURL = 'http://127.0.0.1:8000';

  constructor(
    public http: HttpClient
  ) { }

  getElders() {
    return this.http.get<User[]>(this.baseURL + '/api/elders');
  }

  getProfileElder(id: number) {
    return this.http.get<User>(this.baseURL + '/api/elder-for-profile/' + id);
  }

  getElder(id: number) {
    return this.http.get<User>(this.baseURL + '/api/elder/' + id);
  }

  updateCaretaker(elderId: number, caretakerId: number) {
    return this.http.put(this.baseURL + '/api/assign-caretaker', {
      elderId: elderId,
      caretakerId: caretakerId
    });
  }

  changeRole(userId: number, roleId: number) {
    return this.http.post(this.baseURL + '/api/change-role', {
      userId: userId,
      roleId: roleId
    });
  }
}
