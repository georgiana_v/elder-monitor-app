import { Component, OnInit } from '@angular/core';
import { MDBModalRef } from 'ng-uikit-pro-standard';
import { Subject } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ElderService } from '../elder.service';

@Component({
  selector: 'app-assign-caretaker-modal',
  templateUrl: './assign-caretaker-modal.component.html',
  styleUrls: ['./assign-caretaker-modal.component.scss']
})
export class AssignCaretakerModalComponent implements OnInit {
  public UpdateTrigger: Subject<any> = new Subject<any>();

  public elder: User;
  public caretakers: User[];

  caretakerOptions: { value: number, label: string}[] = [];

  assignCaretakerForm: FormGroup;

  constructor(
    public modalRef: MDBModalRef,
    public fb: FormBuilder,
    public elderService: ElderService
  ) {}

  ngOnInit(): void {
    this.caretakerOptions = this.caretakers.filter(caretaker => caretaker.role_id == 2).map(caretaker => {
      return {
        value: caretaker.id,
        label: caretaker.name
      }
    })

    this.assignCaretakerForm = this.fb.group({
      caretaker: ['', Validators.required]
    })

    if(this.elder.caretaker_id) {
      this.assignCaretakerForm.patchValue({
        caretaker: this.elder.caretaker_id
      })
    }
  }

  onSubmit(form: FormGroup) {
    const caretaker = form.value.caretaker;
    const caretakerName = this.caretakerOptions.find(option => option.value == caretaker).label;

    this.elderService.updateCaretaker(this.elder.id, caretaker).subscribe(() => {
      this.UpdateTrigger.next(caretakerName);
      this.modalRef.hide();
    })
  }

}
