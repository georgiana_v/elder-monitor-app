import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { ActivityComponent } from './activity/activity.component';
import { HeartComponent } from './heart/heart.component';
import { SleepComponent } from './sleep/sleep.component';
import { AuthGuard } from './auth/auth.guard';
import { HomeComponent } from './home/home.component';
import { EldersComponent } from './elders/elders.component';
import { AdminGuard } from './auth/admin.guard';
import { ElderProfileComponent } from './elder-profile/elder-profile.component';
import { CaretakersComponent } from './caretakers/caretakers.component';
import { MyEldersComponent } from './my-elders/my-elders.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'activity', component: ActivityComponent, canActivate: [AuthGuard] },
  { path: 'heart', component: HeartComponent, canActivate: [AuthGuard] },
  { path: 'sleep', component: SleepComponent, canActivate: [AuthGuard] },
  { path: 'elders', component: EldersComponent, canActivate: [AdminGuard] },
  { path: 'my-elders', component: MyEldersComponent, canActivate: [AdminGuard] },
  { path: 'caretakers', component: CaretakersComponent, canActivate: [AdminGuard] },
  { path: 'elder-profile/:id', component: ElderProfileComponent, canActivate: [AdminGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
