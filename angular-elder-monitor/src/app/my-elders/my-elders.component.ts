import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { MdbTableDirective, MdbTablePaginationComponent, MDBModalRef, MDBModalService, ToastService } from 'ng-uikit-pro-standard';
import { User } from '../models/user.model';
import { ElderService } from '../elders/elder.service';
import * as moment from 'moment/moment';
import { AssignCaretakerModalComponent } from '../elders/assign-caretaker-modal/assign-caretaker-modal.component';

@Component({
  selector: 'app-my-elders',
  templateUrl: './my-elders.component.html',
  styleUrls: ['./my-elders.component.scss']
})
export class MyEldersComponent implements OnInit, AfterViewInit {
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild('row', { static: true }) row: ElementRef;

  isLoading = false;

  elders: User[];
  caretakers: User[];
  currentUser: User;

  elements: any = [];
  headElements = ['id', 'name', 'heart Rate', 'sleep Hours', 'steps', 'distance', 'location', 'caretaker'];

  searchText: string = '';
  previous: string;

  maxVisibleItems: number = 25;

  modalRef: MDBModalRef;

  constructor(
    public elderService: ElderService,
    private cdRef: ChangeDetectorRef,
    private modalService: MDBModalService,
    private toast: ToastService
  ) { }

  ngOnInit(): void {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
    this.isLoading = true;

    this.elderService.getElders().subscribe(users => {
      this.elders = users.filter(user => user.role_id == 1 && user.caretaker_id == this.currentUser.id);

      this.caretakers = users.filter(user => user.role_id == 2);

      this.elders.forEach(elder => {
        const todayActivities = elder.activities.filter(activity => moment().format('DM') == moment(activity.created_at).format('DM'));
        const todaySteps = todayActivities.map(activity => activity.hour_steps).reduce((a, b) => +a + +b, 0);
        const todayDistance = todayActivities.map(activity => activity.hour_distance).reduce((a, b) => +a + +b, 0) * 1000;
        const todayHeartRates = elder.heart_rates.filter(rate => moment().format('DM') == moment(rate.created_at).format('DM'));
        const todaySleep = elder.sleep_hours.filter(sleep => moment().format('DM') == moment(sleep.created_at).format('DM'));
        const random = Math.random() * 10;

        this.elements.push({
          id: elder.id,
          name: elder.name,
          heart_rate: todayHeartRates[todayHeartRates.length - 1].heart_rate,
          sleep_hours: todaySleep[todaySleep.length - 1].sleeping_hours,
          steps: todaySteps,
          distance: todayDistance.toFixed(0),
          location: random > 3 ? true : false,
          caretaker: elder.caretaker_id ? elder.caretaker.name : null
        })
      })

      this.mdbTable.setDataSource(this.elements);
      this.elements = this.mdbTable.getDataSource();
      this.previous = this.mdbTable.getDataSource();

      this.isLoading = false;
    })
  }

  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(this.maxVisibleItems);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  searchItems() {
    const prev = this.mdbTable.getDataSource();

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();

    this.mdbTable.searchDataObservable(this.searchText).subscribe(() => {
      this.mdbTablePagination.calculateFirstItemIndex();
      this.mdbTablePagination.calculateLastItemIndex();
    });
  }

  assignCaretaker(elderId: number) {
    const elder = this.elders.find(elder => elder.id == elderId);
    const elIndex = this.elements.findIndex(el => el.id == elder.id);

    this.modalRef = this.modalService.show(AssignCaretakerModalComponent, {
      data: {
        elder: elder,
        caretakers: this.caretakers,
      },
      class: 'modal-dialog-centered modal-lg',
      animated: true
    });

    this.modalRef.content.UpdateTrigger.subscribe((caretakerName: string) => {
      this.isLoading = true;

      this.elements = [];

      this.elderService.getElders().subscribe(users => {
        this.elders = users.filter(user => user.role_id == 1 && user.caretaker_id == this.currentUser.id);

        this.elders.forEach(elder => {
          const todayActivities = elder.activities.filter(activity => moment().format('DM') == moment(activity.created_at).format('DM'));
          const todaySteps = todayActivities.map(activity => activity.hour_steps).reduce((a, b) => +a + +b, 0);
          const todayDistance = todayActivities.map(activity => activity.hour_distance).reduce((a, b) => +a + +b, 0) * 1000;
          const todayHeartRates = elder.heart_rates.filter(rate => moment().format('DM') == moment(rate.created_at).format('DM'));
          const todaySleep = elder.sleep_hours.filter(sleep => moment().format('DM') == moment(sleep.created_at).format('DM'));
          const random = Math.random() * 10;

          this.elements.push({
            id: elder.id,
            name: elder.name,
            heart_rate: todayHeartRates[todayHeartRates.length - 1].heart_rate,
            sleep_hours: todaySleep[todaySleep.length - 1].sleeping_hours,
            steps: todaySteps,
            distance: todayDistance.toFixed(0),
            location: random > 3 ? true : false,
            caretaker: elder.caretaker_id ? elder.caretaker.name : null
          })
        });

        this.mdbTable.setDataSource(this.elements);
        this.elements = this.mdbTable.getDataSource();
        this.previous = this.mdbTable.getDataSource();

        this.isLoading = false;
      });

      const options = { positionClass: 'md-toast-bottom-right', opacity: 1 };
      this.toast.success('Caretaker has been assigned succesfully', '', options);

      this.ngAfterViewInit();
    });
  }

}
