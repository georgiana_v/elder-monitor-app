import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MDBModalRef } from 'ng-uikit-pro-standard';
import { ElderService } from 'src/app/elders/elder.service';

@Component({
  selector: 'app-assign-elder-modal',
  templateUrl: './assign-elder-modal.component.html',
  styleUrls: ['./assign-elder-modal.component.scss']
})
export class AssignElderModalComponent implements OnInit {
  public UpdateTrigger: Subject<any> = new Subject<any>();

  public caretakerId: number;
  public elders: User[];

  elderOptions: { value: number, label: string }[] = [];

  assignElderForm: FormGroup;

  constructor(
    public modalRef: MDBModalRef,
    public fb: FormBuilder,
    public elderService: ElderService
  ) { }

  ngOnInit(): void {
    this.elderOptions = this.elders.map(elder => {
      return {
        value: elder.id,
        label: elder.name
      }
    })

    this.assignElderForm = this.fb.group({
      elder: ['', Validators.required]
    })
  }

  onSubmit(form: FormGroup) {
    const elder = form.value.elder;

    this.elderService.updateCaretaker(elder, this.caretakerId).subscribe(() => {
      this.UpdateTrigger.next(elder);
      this.modalRef.hide();
    })
  }

}
