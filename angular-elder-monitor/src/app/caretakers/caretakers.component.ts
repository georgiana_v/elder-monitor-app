import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MdbTableDirective, MdbTablePaginationComponent, MDBModalRef, MDBModalService, ToastService } from 'ng-uikit-pro-standard';
import { User } from '../models/user.model';
import { ElderService } from '../elders/elder.service';
import * as moment from 'moment/moment';
import { AssignElderModalComponent } from './assign-elder-modal/assign-elder-modal.component';
import { CreateCaretakerModalComponent } from './create-caretaker-modal/create-caretaker-modal.component';

@Component({
  selector: 'app-caretakers',
  templateUrl: './caretakers.component.html',
  styleUrls: ['./caretakers.component.scss']
})
export class CaretakersComponent implements OnInit, AfterViewInit {
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild('row', { static: true }) row: ElementRef;

  isLoading = false;

  elders: User[];
  caretakers: User[];

  elements: any = [];
  headElements = ['id', 'name', 'email', 'phone', 'elders assigned', 'actions'];

  searchText: string = '';
  previous: string;

  maxVisibleItems: number = 25;

  modalRef: MDBModalRef;

  constructor(
    public elderService: ElderService,
    private cdRef: ChangeDetectorRef,
    private modalService: MDBModalService,
    private toast: ToastService
  ) { }

  getEldersAndCaretakers() {
    this.elderService.getElders().subscribe(users => {
      this.elders = users.filter(user => user.role_id == 1);
      this.caretakers = users.filter(user => user.role_id == 2);

      this.caretakers.forEach(caretaker => {

        const caretakerElders = this.elders.filter(elder => caretaker.elders.map(elder => elder.id).includes(elder.id));

        this.elements.push({
          id: caretaker.id,
          name: caretaker.name,
          email: caretaker.email,
          phone: caretaker.phone,
          elders_count: caretaker.elders.length,
          collapsed: true,
          elders: caretakerElders.map(elder => {
            return {
              id: elder.id,
              name: elder.name,
              heart_rate: elder.heart_rates.filter(rate => moment().format('DM') == moment(rate.created_at).format('DM'))[elder.heart_rates.filter(rate => moment().format('DM') == moment(rate.created_at).format('DM')).length - 1].heart_rate,
              sleep_hours: elder.sleep_hours.filter(sleep => moment().format('DM') == moment(sleep.created_at).format('DM'))[elder.sleep_hours.filter(sleep => moment().format('DM') == moment(sleep.created_at).format('DM')).length - 1].sleeping_hours,
              steps: elder.activities.filter(activity => moment().format('DM') == moment(activity.created_at).format('DM')).map(activity => activity.hour_steps).reduce((a, b) => +a + +b, 0),
              distance: (elder.activities.filter(activity => moment().format('DM') == moment(activity.created_at).format('DM')).map(activity => activity.hour_distance).reduce((a, b) => +a + +b, 0) * 1000).toFixed(0),
              location: (Math.random() * 10) > 3 ? true : false
            }
          })
        })
      })

      this.mdbTable.setDataSource(this.elements);
      this.elements = this.mdbTable.getDataSource();
      this.previous = this.mdbTable.getDataSource();

      this.isLoading = false;
    })
  }

  ngOnInit(): void {
    this.isLoading = true;

    this.getEldersAndCaretakers();
  }

  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(this.maxVisibleItems);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  searchItems() {
    const prev = this.mdbTable.getDataSource();

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();

    this.mdbTable.searchDataObservable(this.searchText).subscribe(() => {
      this.mdbTablePagination.calculateFirstItemIndex();
      this.mdbTablePagination.calculateLastItemIndex();
    });
  }

  assignElder(caretakerId: number) {
    const caretaker = this.caretakers.find(caretaker => caretaker.id == caretakerId);
    const elders = this.elders.filter(elder => elder.role_id == 1 && !caretaker.elders.map(elder => elder.id).includes(elder.id));

    this.modalRef = this.modalService.show(AssignElderModalComponent, {
      data: {
        caretakerId: caretakerId,
        elders: elders,
      },
      class: 'modal-dialog-centered modal-lg',
      animated: true
    });

    this.modalRef.content.UpdateTrigger.subscribe(id => {
      this.elements = [];

      this.isLoading = true;

      this.getEldersAndCaretakers();

      const options = { positionClass: 'md-toast-bottom-right', opacity: 1 };
      this.toast.success('Elder has been assigned succesfully', '', options);
    })

    this.ngAfterViewInit();
  }

  selectCaretaker() {
    this.modalRef = this.modalService.show(CreateCaretakerModalComponent, {
      data: {
        elders: this.elders,
      },
      class: 'modal-dialog-centered modal-lg',
      animated: true
    });

    this.modalRef.content.UpdateTrigger.subscribe(id => {
      this.elements = [];

      this.isLoading = true;

      this.getEldersAndCaretakers();

      const options = { positionClass: 'md-toast-bottom-right', opacity: 1 };
      this.toast.success('Caretaker has been selected succesfully', '', options);
    })

    this.ngAfterViewInit();
  }
}
