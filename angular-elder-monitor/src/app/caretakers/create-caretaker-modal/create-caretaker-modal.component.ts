import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MDBModalRef } from 'ng-uikit-pro-standard';
import { ElderService } from '../../elders/elder.service';

@Component({
  selector: 'app-create-caretaker-modal',
  templateUrl: './create-caretaker-modal.component.html',
  styleUrls: ['./create-caretaker-modal.component.scss']
})
export class CreateCaretakerModalComponent implements OnInit {
  public UpdateTrigger: Subject<any> = new Subject<any>();

  public elders: User[];

  caretakerOptions: { value: number, label: string}[] = [];

  selectCaretakerForm: FormGroup;

  constructor(
    public modalRef: MDBModalRef,
    public fb: FormBuilder,
    public elderService: ElderService
  ) { }

  ngOnInit(): void {
    this.caretakerOptions = this.elders.map(elder => {
      return {
        value: elder.id,
        label: elder.name
      }
    })

    this.selectCaretakerForm = this.fb.group({
      caretaker: ['', Validators.required]
    })
  }

  onSubmit(form: FormGroup) {
    const caretaker = form.value.caretaker;

    this.elderService.changeRole(caretaker, 2).subscribe(_res => {
      this.UpdateTrigger.next(caretaker);
      this.modalRef.hide();
    })
  }

}
