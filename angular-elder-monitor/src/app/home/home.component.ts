import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { User } from '../models/user.model';
import { ElderService } from '../elders/elder.service';
import * as moment from 'moment';
import { Activity } from '../models/activity.model';
import { Heart } from '../models/heart.model';
import { Sleep } from '../models/sleep.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  isAdmin: boolean;
  elders: User[] = [];
  todayActivities: Activity[] = null;
  todayHeartRates: Heart[] = null;
  todaySleepHours: Sleep[] = null;
  heartRateAlerts: { user_id: number, location: boolean, heart_rate: number, min: number, max: number, average: number, name: string, date: moment.Moment }[] = [];

  constructor(
    public authService: AuthService,
    public elderService: ElderService
  ) {
    const user = JSON.parse(localStorage.getItem('user'));

    if(user && user.role.name == 'Caretaker'){
      this.isAdmin = true;
    } else {
      this.isAdmin = false;
    }
  }

  ngOnInit(): void {
    if(this.isAdmin) {
      this.elderService.getElders().subscribe(elders => {

        elders.filter(elder => elder.role_id == 1).forEach(elder => {
          elder.heart_rates = elder.heart_rates.filter(heart_rate => heart_rate.heart_rate > 147);

          elder.heart_rates.forEach(heart_rate => {
            const random = Math.random() * 10;

            this.heartRateAlerts.push({
              user_id: heart_rate.user_id,
              location: random > 3 ? true : false,
              heart_rate: heart_rate.heart_rate,
              min: heart_rate.daily_min,
              max: heart_rate.daily_max,
              average: heart_rate.daily_average,
              name: elder.name,
              date: moment(heart_rate.created_at)
            })
          });

          if(elder.heart_rates.length > 1) {
            this.elders.push(elder);
          }
        })
      })
    } else {
      this.elderService.getProfileElder(JSON.parse(localStorage.getItem('user')).id).subscribe(elder => {
        this.todayActivities = elder.activities.filter(activity => moment().format('DM') == moment(activity.created_at).format('DM'));
        this.todayHeartRates = elder.heart_rates.filter(heart_rate => moment().format('DM') == moment(heart_rate.created_at).format('DM'));
        this.todaySleepHours = elder.sleep_hours.filter(sleep_hours => moment().format('DM') == moment(sleep_hours.created_at).format('DM'));
      })
    }
  }

  averageHeartRate(heartRates: Heart[]) {
    const rates = heartRates.map(heart => heart.daily_average);

    const average = rates.reduce((a, b) => a + b, 0) / rates.length;

    return average.toFixed(0);
  }

  averageActivity(activities: Activity[]) {
    const steps = activities.map(activity => activity.daily_steps);
    const distance = activities.map(activity => activity.daily_distance);

    const averageSteps = steps.reduce((a, b) => a + b, 0) / steps.length;
    const averageDistance = distance.reduce((a, b) => +a + +b, 0) / distance.length * 1000;

    const average = { averageSteps: averageSteps.toFixed(0), averageDistance: averageDistance.toFixed(2)};

    return average;
  }

}
