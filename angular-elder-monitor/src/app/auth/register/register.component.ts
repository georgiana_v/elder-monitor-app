import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MustMatch } from '../validators';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  isLoading: boolean;

  registerForm: FormGroup;

  genderOptions: { value: string, label: string}[] = [
    { value: 'male',
      label: 'male'
    },
    { value: 'female',
      label: 'female'
    }
  ];

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      name: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      gender: ['', Validators.required],
      password: ['', Validators.required],
      password_confirm: ['', Validators.required]
    }, {
      validator: MustMatch('password', 'password_confirm')
    });
  }

  onSubmit(form: FormGroup) {
    const name = form.value.name;
    const username = form.value.username;
    const email = form.value.email;
    const phone = form.value.phone;
    const gender = form.value.gender;
    const password = form.value.password;

    this.authService.register(name, email, username, phone, gender, password);
  }
}
