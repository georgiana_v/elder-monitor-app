import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({ providedIn: 'root' })
export class AdminGuard implements CanActivate {
  constructor(
    public authService: AuthService,
    public route: Router
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
   const user = JSON.parse(localStorage.getItem('user'));

    if(user && user.role.name == 'Caretaker') {
      return true;
    } else {
      this.route.navigate(['home']);
    }
  }
}
