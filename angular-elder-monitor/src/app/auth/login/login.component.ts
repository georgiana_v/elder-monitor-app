import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  error: Observable<string>;

  loginForm: FormGroup;

  constructor(
    public authService: AuthService,
    public fb: FormBuilder,
    public router: Router
  ) {
    this.error = this.authService.error;
  }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  onSubmit(form: FormGroup) {

    const username = form.value.username;
    const password = form.value.password;

    this.authService.login(username, password)
  }

}
