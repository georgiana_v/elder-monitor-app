import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    public route: Router
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
   const auth = localStorage.getItem('isAuth');

    if(auth && auth == 'true') {
      return true;
    } else {
      this.route.navigate(['login']);
    }
  }
}
