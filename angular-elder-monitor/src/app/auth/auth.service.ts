import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseURL = 'http://127.0.0.1:8000';
  errorSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  userSubject: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  isAuthSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isAuth: Observable<boolean>;
  user: Observable<User>;
  error: Observable<string>;

  constructor(
    public http: HttpClient,
    public router: Router
  ) {
    this.isAuth = this.isAuthSubject.asObservable();
    this.user = this.userSubject.asObservable();
    this.error = this.errorSubject.asObservable();
   }

  public get isAuthValue(): boolean {
    return this.isAuthSubject.value;
  }

  public get currentUser(): User {
    return this.userSubject.value;
  }

  public get errorValue(): string {
    return this.errorSubject.value;
  }

  initAuthListener() {
    this.http.get(this.baseURL + '/api/user').subscribe((user: User) => {
      if(user) {
        localStorage.setItem('user', JSON.stringify(user));
        localStorage.setItem('isAuth', 'true');
        this.isAuthSubject.next(true);
        this.userSubject.next(user);
        if(this.router.url == '/login'){
          this.router.navigate(['profile']);
        }
      } else {
        localStorage.removeItem('user');
        localStorage.setItem('isAuth', 'false');
        this.isAuthSubject.next(false);
        this.userSubject.next(null);
        this.router.navigate(['login']);
      }
    },
    error => {
      localStorage.removeItem('user');
      localStorage.setItem('isAuth', 'false');
      this.isAuthSubject.next(false);
      this.userSubject.next(null);
      this.router.navigate(['login']);
    })
  }

  register(
    name: string,
    email: string,
    username: string,
    phone: string,
    gender: string,
    password: string
    ) {
    this.http.get(this.baseURL + '/sanctum/csrf-cookie').toPromise().then(response => {
      this.http.post(this.baseURL + '/register', {
        name: name,
        email: email,
        username: username,
        phone: phone,
        gender: gender,
        password: password,
      }).toPromise().then(result => {
        this.initAuthListener();
      },
      error =>{
        this.errorSubject.next(error.error.error);
      });
    });
  }

  login(username: string, password:string) {
    this.http.get(this.baseURL + '/sanctum/csrf-cookie').toPromise().then(response => {
      this.http.post(this.baseURL + '/login', {
        username: username,
        password: password,
      }).toPromise().then(result => {
        this.initAuthListener();
      },
      error => {
        this.errorSubject.next(error.error.error);
      });
    });
  }

  logout() {
    this.http.post(this.baseURL + '/logout', '').subscribe(response => {
      this.initAuthListener();
    })
  }
}
