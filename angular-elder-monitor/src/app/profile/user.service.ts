import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseURL = 'http://127.0.0.1:8000';

  constructor(
    public http: HttpClient
  ) { }

    updateUser(
      id: number,
      name: string,
      username: string,
      email: string,
      phone: string,
      gender: string,
      blood_type: string,
      weight: string,
      height: string,
      password: string,
    ) {
      return this.http.put<User>(this.baseURL + '/api/user', {
        id: id,
        name: name,
        username: username,
        email: email,
        phone: phone,
        gender: gender,
        blood_type: blood_type,
        weight: weight,
        height: height,
        password: password
      });
    }
}
