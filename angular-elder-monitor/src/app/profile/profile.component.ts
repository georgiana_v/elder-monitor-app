import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from './user.service';
import { MustMatch } from '../auth/validators';
import { AuthService } from '../auth/auth.service';
import { User } from '../models/user.model';
import { Activity } from '../models/activity.model';
import * as moment from 'moment/moment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  currentUser: User;
  editMode = false;
  showAlert = false;
  alertMessage = '';

  profileForm: FormGroup;

  genderOptions: { value: string, label: string}[] = [
    { value: 'male', label: 'male' },
    { value: 'female', label: 'female' }
  ];

  bloodTypeOptions: { value: string, label: string }[] = [
    { value: 'A+', label: 'A+' },
    { value: 'A-', label: 'A-' },
    { value: 'B+', label: 'B+' },
    { value: 'B-', label: 'B-' },
    { value: 'O+', label: 'O+' },
    { value: 'O-', label: 'O-' },
    { value: 'AB+', label: 'AB+' },
    { value: 'AB-', label: 'AB-' }
  ]

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
  ) {
    this.currentUser = JSON.parse(localStorage.getItem('user'));
   }

  ngOnInit(): void {
    this.profileForm = this.fb.group({
      name: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      gender: ['', Validators.required],
      blood_type: [''],
      weight: ['', Validators.min(0)],
      height: ['', Validators.min(0)],
      password: [''],
      password_confirm: ['']
    }, {
      validator: MustMatch('password', 'password_confirm')
    });

    if(this.currentUser) {
      this.profileForm.patchValue({
        name: this.currentUser.name,
        username: this.currentUser.username,
        email: this.currentUser.email,
        phone: this.currentUser.phone,
        gender: this.currentUser.gender,
        blood_type: this.currentUser.blood_type,
        weight: this.currentUser.weight,
        height: this.currentUser.height
      })
    }
  }

  onSubmit(form: FormGroup) {
    const name = form.value.name;
    const username = form.value.username;
    const email = form.value.email;
    const phone = form.value.phone;
    const gender = form.value.gender;
    const blood_type = form.value.blood_type;
    const weight = form.value.weight;
    const height = form.value.height;
    const password = form.value.password;

    this.userService.updateUser(
      this.currentUser.id,
      name,
      username,
      email,
      phone,
      gender,
      blood_type,
      weight,
      height,
      password
    ).subscribe(user => {
      this.alertMessage = 'Profilul a fost actualizat cu succes!';
      this.showAlert = true;
      setTimeout(() => {
        this.showAlert = false;
      }, 2500);

      this.currentUser = user;
    })
  }

  calculateStepsAndDistance(activities: Activity[]) {
    const todayActivities = activities.filter(activity => moment().format('DM') == moment(activity.created_at).format('DM'))

    const todayTotalSteps = todayActivities.map(activity => activity.hour_steps).reduce((a, b) => +a + +b, 0);
    const todayTotalDistance = todayActivities.map(activity => activity.hour_distance).reduce((a, b) => +a + +b, 0) * 1000;

    return {
      totalSteps: todayTotalSteps.toFixed(2),
      totalDistance: todayTotalDistance.toFixed(2)
    }
  }

}
