import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef, HostListener } from '@angular/core';
import { MdbTableDirective, MdbTablePaginationComponent } from 'ng-uikit-pro-standard';
import { ActivityService } from './activity.service';
import * as moment from 'moment/moment';
import { Activity } from '../models/activity.model';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit, AfterViewInit {
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild('row', { static: true }) row: ElementRef;
  hourlyChart = true;
  isLoading = false;

  activities: Activity[] = [];

  public chartType: string = 'line';

  public chartDatasets: {data: any[], label: string }[] = [
    { data: [], label: 'Steps/Hour'},
    { data: [], label: 'Distance/Hour'}
  ];
  public chartLabels: Array<any> = [];

  public chartOptions: any = {
    responsive: true
  };

  public chartColors: Array<any> = [
    {
      backgroundColor: 'rgba(76, 175, 80, 0.3)',
      borderColor: 'rgba(76, 175, 80, 0.7)',
      borderWidth: 2,
    },
    {
      backgroundColor: 'rgba(3, 169, 244, 0.3)',
      borderColor: 'rgba(3, 169, 244, 0.7)',
      borderWidth: 2,
    }
  ];

  elements: any = [];
  headElements = ['id', 'steps/Hour', 'distance/Hour', 'steps/Day', 'distance/Day', 'date - Time'];

  searchText: string = '';
  previous: string;

  maxVisibleItems: number = 25;

  constructor(
    private cdRef: ChangeDetectorRef,
    public activityService: ActivityService
  ) { }

  @HostListener('input') oninput() {
    this.mdbTablePagination.searchText = this.searchText;
  }

  ngOnInit(): void {
    this.isLoading = true;

    const user = JSON.parse(localStorage.getItem('user'));

    this.activityService.getUserActivities(user.id).subscribe(activities => {
      this.activities = activities;

      activities.forEach(activity => {
        this.elements.push({
          id: activity.id,
          steps_hour: activity.hour_steps,
          distance_hour: activity.hour_distance * 1000,
          steps_day: activity.daily_steps,
          distance_day: activity.daily_distance * 1000,
          date_time:  moment(activity.created_at).format('MMM DD - HH:mm')
        })
      })

      this.mdbTable.setDataSource(this.elements);
      this.elements = this.mdbTable.getDataSource();
      this.previous = this.mdbTable.getDataSource();

      this.changeChartData();

      this.isLoading = false;
    })
  }

  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(this.maxVisibleItems);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  searchItems() {
    const prev = this.mdbTable.getDataSource();

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();

    this.mdbTable.searchDataObservable(this.searchText).subscribe(() => {
      this.mdbTablePagination.calculateFirstItemIndex();
      this.mdbTablePagination.calculateLastItemIndex();
    });
  }

  changeChartData() {
    if(this.hourlyChart) {
      const todayActivities = this.activities.filter(activity => moment().format('DM') == moment(activity.created_at).format('DM'));

      this.chartDatasets[0].data = todayActivities.map(activity => activity.hour_steps);
      this.chartDatasets[0].label = 'Steps/Hour';
      this.chartDatasets[1].data = todayActivities.map(activity => activity.hour_distance * 1000);
      this.chartDatasets[1].label = 'Meters/Hour';
      this.chartLabels = todayActivities.map(activity => moment(activity.created_at).format('HH:mm'));
    } else {
      // this.chartDatasets[0].data = [...new Set(this.activities.map(activity => activity.daily_steps))];
      this.chartDatasets[0].label = 'Steps/Day';
      // this.chartDatasets[1].data = [...new Set(this.activities.map(activity => activity.daily_distance * 1000))];
      this.chartDatasets[1].label = 'Meters/Day';
      this.chartLabels = [...new Set(this.activities.map(activity => moment(activity.created_at).format('DD MMM')))];
      this.chartDatasets[0].data = [];
      this.chartDatasets[1].data = [];
      this.chartLabels.forEach(day => {
        this.chartDatasets[0].data.push(this.activities.filter(activity => moment(activity.created_at).format('DD MMM') == day)[0].daily_steps);
        this.chartDatasets[1].data.push(this.activities.filter(activity => moment(activity.created_at).format('DD MMM') == day)[0].daily_distance * 1000);
      })
    }
    this.hourlyChart = !this.hourlyChart;
  }
}
