import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Activity } from '../models/activity.model';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {
  baseURL = 'http://127.0.0.1:8000';

  constructor(
    public http: HttpClient
  ) { }

  getUserActivities(id: number) {
    return this.http.get<Activity[]>(this.baseURL + '/api/activities/' + id);
  }
}
