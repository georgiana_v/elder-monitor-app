import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MDBBootstrapModulesPro, ToastModule } from 'ng-uikit-pro-standard';
import { MDBSpinningPreloader } from 'ng-uikit-pro-standard';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SleepComponent } from './sleep/sleep.component';
import { HeartComponent } from './heart/heart.component';
import { ActivityComponent } from './activity/activity.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptorService } from './auth/auth-interceptor.service';
import { HomeComponent } from './home/home.component';
import { EldersComponent } from './elders/elders.component';
import { ElderProfileComponent } from './elder-profile/elder-profile.component';
import { AssignCaretakerModalComponent } from './elders/assign-caretaker-modal/assign-caretaker-modal.component';
import { CaretakersComponent } from './caretakers/caretakers.component';
import { CreateCaretakerModalComponent } from './caretakers/create-caretaker-modal/create-caretaker-modal.component';
import { AssignElderModalComponent } from './caretakers/assign-elder-modal/assign-elder-modal.component';
import { MyEldersComponent } from './my-elders/my-elders.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SleepComponent,
    HeartComponent,
    ActivityComponent,
    ProfileComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    EldersComponent,
    ElderProfileComponent,
    AssignCaretakerModalComponent,
    CaretakersComponent,
    CreateCaretakerModalComponent,
    AssignElderModalComponent,
    MyEldersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MDBBootstrapModulesPro.forRoot(),
    ToastModule.forRoot(),
  ],
  entryComponents: [
    AssignCaretakerModalComponent,
    CreateCaretakerModalComponent,
    AssignElderModalComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
    MDBSpinningPreloader,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
