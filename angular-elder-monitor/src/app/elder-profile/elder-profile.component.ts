import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ElderService } from '../elders/elder.service';
import { mergeMap } from 'rxjs/operators';
import { User } from '../models/user.model';
import { Activity } from '../models/activity.model';
import * as moment from 'moment/moment';

@Component({
  selector: 'app-elder-profile',
  templateUrl: './elder-profile.component.html',
  styleUrls: ['./elder-profile.component.scss']
})
export class ElderProfileComponent implements OnInit {
  elderId: number;
  elder: User;

  public chartType: string = 'line';

  public chartOptions: any = {

  };

  public chartDatasetsActivity: {data: any[], label: string }[] = [
    { data: [], label: 'Steps/Hour'},
    { data: [], label: 'Distance/Hour'}
  ];
  public chartDatasetsHeart: {data: any[], label: string }[] = [
    { data: [], label: 'Heart Rate / Hour'},
    { data: [], label: 'Average BPM / Day'}
  ];
  public chartDatasetsSleep: {data: any[], label: string }[] = [
    { data: [], label: 'Sleep Hours / Day'},
  ];

  public chartLabelsActivity: Array<any> = [];
  public chartLabelsHeart: Array<any> = [];
  public chartLabelsSleep: Array<any> = [];

  public chartColorsActivity: Array<any> = [
    {
      backgroundColor: 'rgba(76, 175, 80, 0.3)',
      borderColor: 'rgba(76, 175, 80, 0.7)',
      borderWidth: 2,
    },
    {
      backgroundColor: 'rgba(3, 169, 244, 0.3)',
      borderColor: 'rgba(3, 169, 244, 0.7)',
      borderWidth: 2,
    }
  ];
  public chartColorsHeart: Array<any> = [
    {
      backgroundColor: 'rgba(244, 67, 54, 0.3)',
      borderColor: 'rgba(244, 67, 54, 0.7)',
      borderWidth: 2,
    },
    {
      backgroundColor: 'rgba(205, 220, 57, 0.3)',
      borderColor: 'rgba(205, 220, 57, 0.7)',
      borderWidth: 2,
    }
  ];
  public chartColorsSleep: Array<any> = [
    {
      backgroundColor: 'rgba(121, 85, 72, 0.3)',
      borderColor: 'rgba(121, 85, 72, 0.7)',
      borderWidth: 2,
    }
  ];

  constructor(
    public route: ActivatedRoute,
    public elderService: ElderService
  ) { }

  ngOnInit(): void {
    this.route.params.
      pipe(mergeMap(params => this.elderService.getElder(params['id']))).subscribe(elder => {
        this.elder = elder;

        const todayActivities = this.elder.activities.filter(activity => moment().format('DM') == moment(activity.created_at).format('DM'));

        this.chartDatasetsActivity[0].data = todayActivities.map(activity => activity.hour_steps);
        this.chartDatasetsActivity[0].label = 'Steps/Hour';
        this.chartDatasetsActivity[1].data = todayActivities.map(activity => activity.hour_distance * 1000);
        this.chartDatasetsActivity[1].label = 'Meters/Hour';
        this.chartLabelsActivity = todayActivities.map(activity => moment(activity.created_at).format('HH:mm'));

        const todayHearts = this.elder.heart_rates.filter(heart => moment().format('DM') == moment(heart.created_at).format('DM'));

        this.chartDatasetsHeart[0].data = todayHearts.map(heart => heart.heart_rate);
        this.chartDatasetsHeart[0].label = 'Heart Rate / Hour';
        this.chartDatasetsHeart[1].data = todayHearts.map(heart => heart.daily_average);
        this.chartDatasetsHeart[1].label = 'Average BPM / Day';
        this.chartLabelsHeart = todayHearts.map(heart => moment(heart.created_at).format('HH:mm'));

        this.chartDatasetsSleep[0].data = this.elder.sleep_hours.map(sleep => sleep.sleeping_hours);
        this.chartLabelsSleep = this.elder.sleep_hours.map(sleep => moment(sleep.created_at).format('DD MMM'));
      });
  }

  calculateStepsAndDistance(activities: Activity[]) {
    const todayActivities = activities.filter(activity => moment().format('DM') == moment(activity.created_at).format('DM'))

    const todayTotalSteps = todayActivities.map(activity => activity.hour_steps).reduce((a, b) => +a + +b, 0);
    const todayTotalDistance = todayActivities.map(activity => activity.hour_distance).reduce((a, b) => +a + +b, 0) * 1000;

    return {
      totalSteps: todayTotalSteps.toFixed(2),
      totalDistance: todayTotalDistance.toFixed(2)
    }
  }

}
