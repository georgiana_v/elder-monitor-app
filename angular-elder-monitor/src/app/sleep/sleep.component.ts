import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, ChangeDetectorRef, HostListener } from '@angular/core';
import { MdbTableDirective, MdbTablePaginationComponent } from 'ng-uikit-pro-standard';
import { Sleep } from '../models/sleep.model';
import { SleepService } from './sleep.service';
import * as moment from 'moment/moment';

@Component({
  selector: 'app-sleep',
  templateUrl: './sleep.component.html',
  styleUrls: ['./sleep.component.scss']
})
export class SleepComponent implements OnInit, AfterViewInit {
  @ViewChild(MdbTableDirective, { static: true }) mdbTable: MdbTableDirective;
  @ViewChild(MdbTablePaginationComponent, { static: true }) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild('row', { static: true }) row: ElementRef;

  isLoading= false;

  sleeps: Sleep[] = [];

  public chartType: string = 'line';

  public chartDatasets: {data: any[], label: string }[] = [
    { data: [], label: 'Sleep Hours / Day'},
  ];

  public chartLabels: Array<any> = [];

  public chartOptions: any = {
    responsive: true
  };

  public chartColors: Array<any> = [
    {
      backgroundColor: 'rgba(121, 85, 72, 0.3)',
      borderColor: 'rgba(121, 85, 72, 0.7)',
      borderWidth: 2,
    }
  ];

  elements: any = [];
  headElements = ['id', 'sleep hours', 'date - Time'];

  searchText: string = '';
  previous: string;

  maxVisibleItems: number = 25;

  constructor(
    private cdRef: ChangeDetectorRef,
    public sleepService: SleepService
  ) { }

  @HostListener('input') oninput() {
    this.mdbTablePagination.searchText = this.searchText;
  }

  ngOnInit(): void {
    this.isLoading = true;

    const user = JSON.parse(localStorage.getItem('user'));

    this.sleepService.getUserSleeps(user.id).subscribe(sleeps => {
      this.sleeps = sleeps;

      sleeps.forEach(sleep => {
        this.elements.push({
          id: sleep.id,
          sleep_hours: sleep.sleeping_hours,
          date_time:  moment(sleep.created_at).format('MMM DD - HH:mm')
        })
      })

      this.mdbTable.setDataSource(this.elements);
      this.elements = this.mdbTable.getDataSource();
      this.previous = this.mdbTable.getDataSource();

      this.chartDatasets[0].data = this.sleeps.map(sleep => sleep.sleeping_hours);
      this.chartLabels = this.sleeps.map(sleep => moment(sleep.created_at).format('DD MMM'));

      this.isLoading = false;
    })
  }

  ngAfterViewInit() {
    this.mdbTablePagination.setMaxVisibleItemsNumberTo(this.maxVisibleItems);

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  searchItems() {
    const prev = this.mdbTable.getDataSource();

    if (!this.searchText) {
      this.mdbTable.setDataSource(this.previous);
      this.elements = this.mdbTable.getDataSource();
    }

    if (this.searchText) {
      this.elements = this.mdbTable.searchLocalDataBy(this.searchText);
      this.mdbTable.setDataSource(prev);
    }

    this.mdbTablePagination.calculateFirstItemIndex();
    this.mdbTablePagination.calculateLastItemIndex();

    this.mdbTable.searchDataObservable(this.searchText).subscribe(() => {
      this.mdbTablePagination.calculateFirstItemIndex();
      this.mdbTablePagination.calculateLastItemIndex();
    });
  }

}
