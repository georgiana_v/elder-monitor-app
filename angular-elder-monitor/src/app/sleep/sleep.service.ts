import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Sleep } from '../models/sleep.model';

@Injectable({
  providedIn: 'root'
})
export class SleepService {
  baseURL = 'http://127.0.0.1:8000';

  constructor(
    public http: HttpClient
  ) { }

  getUserSleeps(id: number) {
    return this.http.get<Sleep[]>(this.baseURL + '/api/sleeps/' + id);
  }
}
